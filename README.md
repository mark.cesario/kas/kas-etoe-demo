This is a GitLab Kubernetes-based project using the KAS agent for pull-based deployments.

Prerequisites

- Kubernetes cluster (cannot be the same as your current cred-based Kubernetes cluster, i.e. push-based)
- Access to kubectl cli
- You can use .nip.io for your ingress or your own domain

You can clone this app and then make the following changes.

- .gitlab/agents/mce-agent : replace mce-agent to your name
- .gitlab/agents/mce-agent/config.yaml : change id: to the path to your project
- .gitlab-ci.yml :
    - change KUBE_CONTEXT to your project:your agent name (line #2)
    - change KUBE_INGRESS_BASE_DOMAIN to your ingress domain or ingress.ip-addres.nip.io
- app-manifest.yaml
    - Change Pod image to your proeject name (line #10)
    - Change host ingress (after dast-demo.) (line #29)    


Appendix

- https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
- https://kind.sigs.k8s.io/docs/user/ingress/
- https://kubernetes.github.io/ingress-nginx/
- https://kubernetes.github.io/ingress-nginx/deploy/
